\contentsline {chapter}{\numberline {1}Wireframes}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Breakdown}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Home Activity}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Add Car Activity}{3}{section.2.2}
\contentsline {section}{\numberline {2.3}Detail Fragment}{4}{section.2.3}
\contentsline {section}{\numberline {2.4}Maintenance Schedule Fragment}{4}{section.2.4}
\contentsline {section}{\numberline {2.5}Completed Maintenance Fragment}{4}{section.2.5}
\contentsline {section}{\numberline {2.6}Gas Stats Fragment}{5}{section.2.6}
\contentsline {section}{\numberline {2.7}Gas Search Fragment}{5}{section.2.7}
\contentsline {chapter}{\numberline {3}Challenges and Solutions}{5}{chapter.3}
\contentsline {chapter}{\numberline {4}Code Repositories}{5}{chapter.4}
